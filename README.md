# Technical test

## Environment

## Frontend

This project was generated with
- [Angular CLI](https://github.com/angular/angular-cli) version  `11.0.7`
- [Npm](https://www.npmjs.com/) version `7.7.1`
- [Node.js](https://nodejs.org/) version `15.12.0`

Go in the folder `/src/main/js/technical-test-frontend`

Installation :
```sh 
npm install
```

```sh 
ng build
```

## Backend

This project was generated with
- Java version `11`
- [Spring Boot](https://spring.io/projects/spring-boot) version `2.4.4`

Installation & run on linux :
```sh 
mvn spring-boot:run
```

Installation & run on windows :
```sh
mvnw spring-boot:run
```

## Use the project

Go on `http://localhost:8080/`
