import {Component, OnInit} from '@angular/core';
import {Solution} from "./models/solution";
import {SolutionService} from "./services/solution.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public solutions!: Solution[];
  public selectedSolution!: Solution | null;
  public selectedSolutionIndex!: number;

  constructor(private solutionService: SolutionService) {
  }

  ngOnInit(): void {
    this.solutionService.getSolutions().subscribe(solutions => { this.solutions = solutions; });
  }

  selectSolution(solution: Solution, i: number): void {
    this.selectedSolution = solution;
    this.selectedSolutionIndex = i;
  }

  unselectSolution(): void {
    this.selectedSolution = null;
  }

  public findSolutions(): void {
    this.unselectSolution();
    this.solutionService.findSolutions().subscribe(solutions => { this.solutions = solutions; });
  }

  public deleteSolution(solutionToDelete: Solution): void {
    this.unselectSolution();
    this.solutions = this.solutions.filter(solution => solution !== solutionToDelete);
    this.solutionService.deleteSolution(solutionToDelete.id).subscribe();
  }

  public deleteSolutions(): void {
    this.unselectSolution();
    this.solutions = [];
    this.solutionService.deleteSolutions().subscribe();
  }

  public putSolution(newSolution: Solution): void {
    this.unselectSolution();
    this.solutionService.putSolution(newSolution).subscribe();
  }

}
