package com.alexis.defranoux.technical.test.backend.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Solution {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String digits;
    private long nanoSecondExecutionTime;

    public Solution(String digits, long nanoSecondExecutionTime) {
        this.digits = digits;
        this.nanoSecondExecutionTime = nanoSecondExecutionTime;
    }

    public Solution() {
    }

    public long getId() {
        return id;
    }

    public String getDigits() {
        return digits;
    }

    public void setDigits(String digits) {
        this.digits = digits;
    }

    public double getNanoSecondExecutionTime() {
        return nanoSecondExecutionTime;
    }

    public void setNanoSecondExecutionTime(long executionTime) {
        this.nanoSecondExecutionTime = executionTime;
    }

}
