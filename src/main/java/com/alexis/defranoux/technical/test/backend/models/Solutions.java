package com.alexis.defranoux.technical.test.backend.models;

import com.alexis.defranoux.technical.test.backend.entities.Solution;
import com.alexis.defranoux.technical.test.backend.repositories.SolutionRepository;

import java.util.ArrayList;
import java.util.List;

public class Solutions {

    private final List<Solution> solutions;

    /**
     * array of unique numbers : 1 2 6 4 7 8 3 5 9
     */
    private final List<Double> uniqueNumbers;


    public Solutions() {
        this.uniqueNumbers = new ArrayList<>();
        this.solutions = new ArrayList<>();
    }


    public List<Solution> findSolutions(SolutionRepository solutionRepository) {
        long startTime = System.nanoTime();
        solutions.clear();
        while (true) {
            randomizeXArray();
            double equation = uniqueNumbers.get(0)+13.0* uniqueNumbers.get(1)/ uniqueNumbers.get(2)+ uniqueNumbers.get(3)+12.0* uniqueNumbers.get(4)- uniqueNumbers.get(5)-11.0+ uniqueNumbers.get(6)* uniqueNumbers.get(7)/ uniqueNumbers.get(8)-10.0;
            if (equation == 66.0) {
                Solution solution = new Solution(toStringSolution(uniqueNumbers), System.nanoTime() - startTime);
                solutionRepository.save(solution);
                solutions.add(solution);
                return solutions;
            }
        }
    }


    /**
     * Randomize the x array of unique numbers : 1 2 6 4 7 8 3 5 9
     */
    private void randomizeXArray() {
        uniqueNumbers.clear();
        int xArraySize = 0;
        while (xArraySize < 9) {
            double randomNumber = (int) Math.floor((Math.random()*9)+1);
            if (!uniqueNumbers.contains(randomNumber)) {
                uniqueNumbers.add(randomNumber);
                xArraySize++;
            }
        }
    }


    private String toStringSolution(List<Double> uniqueNumbers){
        StringBuilder stringSolution = new StringBuilder();
        for (int i = 0; i < uniqueNumbers.size()-1; i++) {
            stringSolution.append(uniqueNumbers.get(i).intValue()).append(", ");
        }
        stringSolution.append(uniqueNumbers.get(8).intValue());
        return stringSolution.toString();
    }

}
