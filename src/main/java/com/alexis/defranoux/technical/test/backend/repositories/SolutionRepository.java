package com.alexis.defranoux.technical.test.backend.repositories;

import com.alexis.defranoux.technical.test.backend.entities.Solution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SolutionRepository extends CrudRepository<Solution, Long>{}