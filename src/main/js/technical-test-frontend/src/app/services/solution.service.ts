import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Solution} from "../models/solution";

@Injectable({
  providedIn: 'root'
})
export class SolutionService {

  private readonly solutionsUrl: string;

  constructor(private http: HttpClient) {
    this.solutionsUrl = 'http://localhost:8080/solutions';
  }

  public findSolutions(): Observable<Solution[]> {
    return this.http.post<Solution[]>(this.solutionsUrl, "");
  }

  public getSolutions(): Observable<Solution[]> {
    return this.http.get<Solution[]>(this.solutionsUrl);
  }

  public deleteSolutions(): Observable<Solution[]> {
    return this.http.delete<Solution[]>(this.solutionsUrl);
  }

  public deleteSolution(id: number): Observable<Solution> {
    return this.http.delete<Solution>(this.solutionsUrl + "/" + id);
  }

  public putSolution(solution: Solution): Observable<Solution> {
    return this.http.put<Solution>(this.solutionsUrl + "/" + solution.id, solution);
  }

}
