package com.alexis.defranoux.technical.test.backend.controllers;

import com.alexis.defranoux.technical.test.backend.entities.Solution;
import com.alexis.defranoux.technical.test.backend.models.Solutions;
import com.alexis.defranoux.technical.test.backend.repositories.SolutionRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController()
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/solutions")
public class SolutionController {


    private final SolutionRepository solutionRepository;
    Solutions solutions;


    public SolutionController(SolutionRepository solutionRepository) {
        this.solutionRepository = solutionRepository;
        solutions = new Solutions();
    }


    @PostMapping()
    public List<Solution> findSolutions() {
        deleteSolutions();
        return solutions.findSolutions(solutionRepository);
    }


    @GetMapping()
    public List<Solution> getSolutions() {
        return (List<Solution>) solutionRepository.findAll();
    }


    @GetMapping("/{id}")
    public Solution getSolutionById(@PathVariable(value = "id") Long solutionId){
        return solutionRepository.findById(solutionId).get();
    }


    @DeleteMapping()
    public void deleteSolutions() {
        solutionRepository.findAll().forEach(solutionRepository::delete);
    }


    @DeleteMapping("/{id}")
    public void deleteSolution(@PathVariable(value = "id") Long solutionId) {
        Optional<Solution> oldSolution = solutionRepository.findById(solutionId);
        solutionRepository.delete(oldSolution.get());
    }


    @PutMapping("/{id}")
    public Solution putSolution(@PathVariable(value = "id") Long solutionId, @RequestBody Solution newSolution) {
        Optional<Solution> oldSolution = solutionRepository.findById(solutionId);
        oldSolution.get().setDigits(newSolution.getDigits());
        return solutionRepository.save(oldSolution.get());
    }

}
